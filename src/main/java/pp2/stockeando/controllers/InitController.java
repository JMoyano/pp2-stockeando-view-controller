package pp2.stockeando.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import pp2.stockeando.model.SearchModel;
import pp2.stockeando.views.InitView;
import pp2.stockeando.views.SearchView;

public class InitController implements ActionListener {

	private InitView view;
	private SearchModel model;
	private SearchView searchView;
	
	public InitController(InitView view, SearchModel model) {
		
		this.view=view;
		this.model=model;

		view.getCloseButton().addActionListener(this);
		view.getSearchButton().addActionListener(this);
	}
	

	@Override
	public void actionPerformed(ActionEvent eventInitController) {
		
	 if(eventInitController.getActionCommand() == view.button1_str) {
		  System.exit(0);
		  return;
	 }
	 	searchView = new SearchView(model);
	 	searchView.getFrame().setVisible(true);
	 	view.getFrame().setVisible(false);
	}


}
