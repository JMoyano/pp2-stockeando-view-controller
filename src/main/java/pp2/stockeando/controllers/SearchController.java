package pp2.stockeando.controllers;

import pp2.stockeando.controller.Controller;
import pp2.stockeando.model.SearchModel;
import pp2.stockeando.model.UserContact;
import pp2.stockeando.views.InitView;
import pp2.stockeando.views.SearchView;

import javax.swing.*;

import java.awt.event.ActionEvent;
import java.util.List;

public class SearchController implements Controller {
	public SearchModel model;
	public SearchView view;


	public SearchController(SearchView view,SearchModel model) {
		this.model=model;
		this.view=view;
		
		view.getBackButton().addActionListener(this);
		view.getSearchButton().addActionListener(this);
		view.PageListUpdate(model.getPages());
		view.SentChannelComboUpdate(model.getChannelsNotifier());
		model.setSubscribeController(this);
	}
	

	@Override
	public void actionPerformed(ActionEvent eventSearchController) {

		 if(eventSearchController.getActionCommand() == view.button1_str) {		
			 goBack(); 
		 }
		 else {
				try {
					String productName = this.view.getProductInformation();

					String notifierName = (String) this.view.getNotifiersCombo().getSelectedItem();

					UserContact userDetails = new UserContact(this.view.getContactInformation());

					List<String> pagesToSearch = this.view.getSelection();

					if (!model.isReceiverInfoValid(notifierName, userDetails)) {
						throw new IllegalArgumentException("Informaci\u00F3n de contacto inv\u00E1lida.");
					}

					int input = JOptionPane.showOptionDialog(this.view.getFrame(),
							"Vas a hacer la siguiente b\u00FAsqueda: \n" + generateMessagePopUp()
									+ "\n\u00BFDesea continuar?",
							"Confirmar b\u00FAsqueda", JOptionPane.OK_OPTION, JOptionPane.INFORMATION_MESSAGE, null,
							null, null);
					if (input == 0) {
						model.searchProduct(pagesToSearch, productName, notifierName, userDetails);

					}
				} catch (Exception e) {
					JOptionPane.showMessageDialog(this.view.getFrame(), e.getMessage());
					e.printStackTrace();
				}
		 }

	}
	
	@SuppressWarnings("unused")
	private void goBack() {
		 InitView initView =new InitView(model);
		 view.getFrame().setVisible(false);
		 initView.getFrame().setVisible(true);
		 InitController initController=new InitController(initView,model);
	}
	
	@Override
	public void updateView() {
		
		view.titleLabel.setText(model.getAction());
		view.productField.setEnabled(false);
		view.contactField.setEnabled(false);
		view.notifiersCombo.setEnabled(false);
		view.searchButton.setEnabled(false);
    	view.pagesList.setEnabled(false);
    	
	}


	private String  generateMessagePopUp() {
		String message = "Producto a buscar: " + this.view.getProductInformation();
		message += "\nP\u00e1ginas donde buscar: " + this.view.getSelection().toString();
		message +=  "\nContacto: " + this.view.getContactInformation();
		return message;
	}

}