package pp2.stockeando.main;

import pp2.stockeando.model.SearchModel;
import pp2.stockeando.views.InitView;

public class Main {

	public static void main(String[] args) {
		InitView view=new InitView(new SearchModel());
		view.getFrame().setVisible(true);
	}

}
