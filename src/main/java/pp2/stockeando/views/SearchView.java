package pp2.stockeando.views;

import pp2.stockeando.controllers.SearchController;
import pp2.stockeando.model.SearchModel;
import pp2.stockeando.utils.FontStyle;

import javax.swing.*;
import java.awt.*;
import java.util.List;

import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

public class SearchView{
	private JFrame frame;
    private JScrollPane scrollPane;
	public JList<String> pagesList;
	private JLabel whereToSearchLabel;
	private JLabel productoLabel;
	private JLabel contactLabel;
    public JLabel titleLabel;
	public JButton backButton;
	public JButton searchButton;
	public JTextField productField;
	public JTextField contactField;
	private JPanel contactPanel;
	private JPanel productPanel;
	private JPanel panel;
	public JComboBox<String> notifiersCombo;
	public String button1_str="Volver";
	public String button2_str="Buscar";
	public String title_str = "Buscar un producto";
	public FontStyle font;;
	public String contact_label_str="Contacto por";
	public String whereToSearchLabel_str="P\u00E1ginas disponibles para la b\u00FAsqueda";
	public String productLbl_str="Nombre del producto";
	public SearchModel associatedModel;
	

    public SearchView(SearchModel associatedModel){
    	
    	this.associatedModel=associatedModel;    	
    	font = new FontStyle("Tahoma",24);
        initFrame();
        SetContentView();
        MakeController(associatedModel);
        
    }
    
    
    
    public void MakeController(SearchModel associatedModel) {
    	SearchController SearchController = new SearchController(this,associatedModel);
    }
    
    private void SetContentView() {
    	
    	
    	setTittle(title_str,font.getName(),font.getSize()); 
        backButton=addButton(button1_str,110, 427, 120, 23,new JButton());
        searchButton=addButton(button2_str,260, 427, 120, 23,new JButton());
        scrollPane=CreateScrollPane(new JScrollPane());      
        whereToSearchLabel = new JLabel(whereToSearchLabel_str);
        panel = new JPanel();
        productPanel = new JPanel();
        contactPanel = new JPanel();
        contactLabel = new JLabel(contact_label_str);
        notifiersCombo = new JComboBox<>();
        contactField = CreateJTextField(new JTextField());
        GroupLayout layout = new GroupLayout(frame.getContentPane());
        GenerateLayout(layout);
        contactPanel.setLayout(new BoxLayout(contactPanel, BoxLayout.X_AXIS));
        productPanel.setLayout(new BoxLayout(productPanel, BoxLayout.X_AXIS));
        productoLabel = new JLabel(productLbl_str); 
        productField = new JTextField();
        productField.setColumns(10);
        FlowLayout fl_panel = new FlowLayout(FlowLayout.CENTER, 5, 5);
        panel.setLayout(fl_panel);
        
        addContentsToPanels();
        
        pagesList = new JList<>();
        pagesList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        scrollPane.setViewportView(pagesList);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        frame.getContentPane().setLayout(layout);
        
    }
    
    private void GenerateLayout(GroupLayout layout) {
    	
        layout.setHorizontalGroup(
        	layout.createParallelGroup(Alignment.TRAILING)
        		.addGroup(layout.createSequentialGroup()
        			.addContainerGap()
        			.addGroup(layout.createParallelGroup(Alignment.LEADING)
        				.addComponent(contactPanel, GroupLayout.DEFAULT_SIZE, 464, Short.MAX_VALUE)
        				.addComponent(productPanel, GroupLayout.DEFAULT_SIZE, 464, Short.MAX_VALUE)
        				.addComponent(titleLabel, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 464, Short.MAX_VALUE)
        				.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 464, Short.MAX_VALUE)
        				.addComponent(panel, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 464, Short.MAX_VALUE)
        				.addComponent(whereToSearchLabel, GroupLayout.DEFAULT_SIZE, 464, Short.MAX_VALUE))
        			.addContainerGap())
        );
        layout.setVerticalGroup(
        	layout.createParallelGroup(Alignment.LEADING)
        		.addGroup(layout.createSequentialGroup()
        			.addContainerGap()
        			.addComponent(titleLabel, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addComponent(productPanel, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addComponent(contactPanel, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
        			.addPreferredGap(ComponentPlacement.UNRELATED)
        			.addComponent(whereToSearchLabel)
        			.addGap(47)
        			.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 191, Short.MAX_VALUE)
        			.addPreferredGap(ComponentPlacement.RELATED)
        			.addComponent(panel, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
        			.addGap(11))
        );
        
    }
    
    private void addContentsToPanels () {

        contactPanel.add(contactLabel);
    	contactPanel.add(notifiersCombo);
    	contactPanel.add(contactField);
    	productPanel.add(productoLabel);
        productPanel.add(productField);
    }
    
    private JTextField CreateJTextField(JTextField contactField){
        contactField = new JTextField();
        contactPanel.add(contactField);
        contactField.setColumns(10);
        
        return contactField;
    }
    
    private JScrollPane CreateScrollPane(JScrollPane scrollPane) {
    	  scrollPane = new JScrollPane();
          scrollPane.setToolTipText("");
          scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          return scrollPane;
	}


	public JFrame getFrame(){
    	return frame;
    }

    private void initFrame() {
        frame = new JFrame("Stockeando");
        frame.getContentPane().setLayout(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 500);
        frame.setLocationRelativeTo(null);
    }

    public JComboBox<String> getNotifiersCombo(){
        return notifiersCombo;
    }

    public JButton getBackButton() {
        return backButton;
    }

    public JButton getSearchButton() {
        return searchButton;
    }


	public List<String> getSelection(){
       return (List<String>) this.pagesList.getSelectedValuesList();
    }

    public String getContactInformation(){
        return this.contactField.getText();
    }

    public String getProductInformation(){
        return this.productField.getText();
    }
    
    private JButton addButton(String text_Button,int x_coor,int y_coor, int width, int height, JButton button) {

	  	button = new JButton(text_Button);
        button.setBounds(x_coor, y_coor, width, height);
        frame.getContentPane().add(button);
        return button;
}

    private void setTittle(String title_text,String font,int fontSize) {
    	
    	titleLabel = new JLabel(title_text);
    	titleLabel.setFont(new Font(font, Font.PLAIN, fontSize));
    	titleLabel.setHorizontalAlignment(SwingConstants.CENTER);

    }
    public void PageListUpdate(List<String> pages) {
    	
    	DefaultListModel<String> listModelClear = new DefaultListModel<String>();
    	listModelClear.removeAllElements();
    	pagesList.setModel(listModelClear);
    	
		DefaultListModel<String> listModel = new DefaultListModel<String>();		
		for(int iteration=0;iteration < pages.size();iteration ++) {
			listModel.add(iteration, pages.get(iteration));
		}
		pagesList.setModel(listModel);
		
    }
    public void SentChannelComboUpdate(List<String> channels) {
    	
    	notifiersCombo.removeAllItems();
		
    	for(int iteration=0;iteration < channels.size();iteration ++) {
			notifiersCombo.addItem(channels.get(iteration));
		}
  
    }


}