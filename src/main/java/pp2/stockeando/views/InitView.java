package pp2.stockeando.views;

import javax.swing.*;
import java.awt.*;
import javax.swing.GroupLayout.Alignment;

import pp2.stockeando.controllers.InitController;
import pp2.stockeando.model.SearchModel;
import pp2.stockeando.utils.FontStyle;

public class InitView{

	private JButton searchButton ;
    private JButton closeButton ;
    private JFrame titleframe;
    private JLabel title;
    public String title_str="Stockeando";
    public String button1_str="Salir";
    public String button2_str="Buscar Stock";
    public FontStyle Titlefont= new FontStyle("Tahoma",24);;
    public SearchModel associatedModel;
    
    public InitView(SearchModel associatedModel){      
    	
    	this.associatedModel=associatedModel;
    	buildView();
        MakeController(associatedModel);
    }
        
    
    public void MakeController(SearchModel associatedModel) {
    	InitController initController = new InitController(this,associatedModel);
    }
    
    
    public JFrame getFrame(){
    	    
    	return titleframe;
    }

    private void buildView() {
    	initFrame();  
    	searchButton=addButton(button1_str,173, 427, 120, 23,new JButton());
    	closeButton=addButton(button2_str,173, 393, 120, 23,new JButton());   
    	
        setTittle(title_str,Titlefont.getName(),Titlefont.getSize()); 
        
        GroupLayout groupLayout=createLayout();
        layoutSetVerticalGroup(groupLayout,title);
    }
    
    private void initFrame() {
        titleframe = new JFrame(title_str);
        titleframe.getContentPane().setLayout(null);
        titleframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        titleframe.setSize(500, 500);
        titleframe.setLocationRelativeTo(null);
    }
    
    private void setTittle(String title_text,String font,int fontSize) {
    	
    	title = new JLabel(title_text);
        title.setFont(new Font(font, Font.PLAIN, fontSize));
        title.setHorizontalAlignment(SwingConstants.CENTER);
    }
   
    private JButton addButton(String text_Button,int x_coor,int y_coor, int width, int height, JButton button) {

    	  	button = new JButton(text_Button);
	        button.setBounds(x_coor, y_coor, width, height);
	        titleframe.getContentPane().add(button);
	        return button;
    }
    
    private GroupLayout createLayout() {
    	
    	GroupLayout layout = new GroupLayout(titleframe.getContentPane());
        layout.setHorizontalGroup(
        	layout.createParallelGroup(Alignment.LEADING)
        		.addGroup(layout.createSequentialGroup()
        			.addContainerGap()
        			.addComponent(title, GroupLayout.DEFAULT_SIZE, 464, Short.MAX_VALUE)
        			.addContainerGap())
        );
		return layout;
        
    }
    
    private void layoutSetVerticalGroup(GroupLayout layout_in, JLabel tittle_lbl) {
    	
    	layout_in.setVerticalGroup(
    			layout_in.createParallelGroup(Alignment.LEADING)
	        		.addGroup(layout_in.createSequentialGroup()
	        			.addContainerGap()
	        			.addComponent(tittle_lbl, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
	        			.addContainerGap(399, Short.MAX_VALUE))
	        );
    	layout_in.setAutoCreateGaps(true);
    	layout_in.setAutoCreateContainerGaps(true);
	        titleframe.getContentPane().setLayout(layout_in);
    }
    
    public JButton getSearchButton() {
    	return searchButton;
    }

    public JButton getCloseButton() {
        return closeButton;
    }

}
